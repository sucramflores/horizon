# Horizon OIT

Description:

It's an Asset Tracking and Management tool designed and developed by Marcus Flores to support IT services and operations at Horizon Home Health and Hospice.

- 

Features:

- Invetory tracking (Servers, network equipment, PCs, laptops, tablets, phones, etc)
- Users management
- Remote access integration (Teamviewer and RDP)
- Passwords management (secure vault)
- KB's for IT staff
- Heartbeat (Servers and Network) 
- Integration with Employees portal




![Your Link Text](https://bitbucket.org/sucramflores/horizon/downloads/Login.jpg)

![Your Link Text](https://bitbucket.org/sucramflores/horizon/downloads/Horizon_OIT.jpg)

![Your Link Text](https://bitbucket.org/sucramflores/horizon/downloads/Devices.jpg)

![Your Link Text](https://bitbucket.org/sucramflores/horizon/downloads/Devices_Details.jpg)




